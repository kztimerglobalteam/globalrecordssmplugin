# GlobalRecords SourceMod Plugin
### GlobalRecords SM Plugin is an **all-in-one SourceMod plugin package** to ensure an easy way of implementing global records into your plugin and/or gamemode!

BUILD: [![CircleCI](https://circleci.com/bb/kztimerglobalteam/globalrecordssmplugin/tree/master.svg?style=svg)](https://circleci.com/bb/kztimerglobalteam/globalrecordssmplugin/tree/master)

---

The purpose of this plugin is to ensure that you can easily implement global records into your plugin.

There is no need for you to implement your own API calls, worrying about responses or anything like that. We've already made it for you.

**Plugins already using GlobalAPI plugin can be found here, check them out!**

* [GOKZ](https://bitbucket.org/kztimerglobalteam/gokz)
* [KZTimer](https://bitbucket.org/kztimerglobalteam/kztimerglobal)

---

### **What** are Global Records?
**Global Records** is a system where servers can submit times globally and make players compete in the global leaderboard for your globally- enabled game mode / plugin

### **Why** Global Records?
* Positively affects your game mode's and/or plugin's visibility and player base
* Your communities are no longer separated between servers


---

## **Installation**

* Download the latest release (GlobalAPI-Core-v.X.X.X and **optionally** GlobalAPI-Jumpstats-v.X.X.X) from the [Downloads](https://bitbucket.org/kztimerglobalteam/globalrecordssmplugin/downloads/)
* Extract the package to `\csgo\`
* Place your API key in a single line to `\csgo\cfg\sourcemod\globalrecords.cfg`

## **Usage**
All usable natives and forwards can be found on the **include files** (A more specific documentation is coming soon)

* [GlobalAPI-Core](https://bitbucket.org/kztimerglobalteam/globalrecordssmplugin/src/109ea2ce33d0a5c968b2a95cab25a7cf9d644d5c/include/GlobalAPI-Core.inc?at=master&fileviewer=file-view-default)
* [GlobalAPI-Jumpstats](https://bitbucket.org/kztimerglobalteam/globalrecordssmplugin/src/109ea2ce33d0a5c968b2a95cab25a7cf9d644d5c/include/GlobalAPI-Jumpstats.inc?at=master&fileviewer=file-view-default)

## **Features**
* Full of natives and forwards to ensure a smooth implementation of global records to your plugin
* **Jumpstats** - Support for jumpstats as a separate module! (**Requires GlobalAPI-Core**)
* **Logging** - Every failed HTTP Request is automatically logged for easy debugging
* **Customizable** - Auto-generated config to enable things like development mode
* **Development** - Full support to change your plugin to use the development API endpoint
* **Updater Support** - Support for updater plugin to automatically update your GlobalAPI plugin

## **Requirements**
* Sourcemod 1.8+
* [SteamWorks](https://forums.alliedmods.net/showthread.php?t=229556)
* [SMJansson](https://forums.alliedmods.net/showthread.php?t=184604)
* **Optional** - [Updater Plugin](https://forums.alliedmods.net/showthread.php?t=169095)

## **Documentation**
* API endpoint documentation and this plugin's documentation **COMING SOON!**
